import React, { useEffect, useState } from "react";
import axios from "../axios";
import "./row.css";
import YouTube from "react-youtube";
import movieTrailer from "movie-trailer";

const base_url = "https://image.tmdb.org/t/p/original/";
const opts = {
  height: "390",
  width: "640",
  playerVars: {
    autoplay: 1,
  },
};

export default function Row({ title, fetchUrl, isLarge }) {
  const [movies, setMovies] = useState([]);
  const [trailerUrl, setTrailerUrl] = useState("");

  useEffect(() => {
    async function fetchData() {
      try {
        const request = await axios.get(fetchUrl);
        setMovies(request.data.results);
        return request;
      } catch (e) {
        console.log(fetchUrl);
      }
    }

    fetchData();
  }, [fetchUrl]);

  async function handleTrailerClick(movie) {
    if (trailerUrl) {
      setTrailerUrl("");
    } else {
      try {
        console.log(movie?.title || movie?.name || movie?.original_name || "")
        const url_id = await movieTrailer(movie?.title || movie?.name || movie?.original_name || "", {id: true});
        // const urlParams = new URLSearchParams(new URL(url).search);
        // setTrailerUrl(urlParams.get('v'))
        setTrailerUrl(url_id)
      } catch (e) {
        console.log(e);
      }
    }
  }

  return (
    <div className="row">
      <h2>{title}</h2>
      {/* container posters and all*/}
      <div className="row__posters">
        {movies.map((movie) => (
          <img
            key={movie.id}
            onClick={() => handleTrailerClick(movie)}
            className={`row__poster ${isLarge && "row__posterLarge"}`}
            src={`${base_url}${
              isLarge ? movie.poster_path : movie.backdrop_path
            }`}
            alt={movie.name}
          />
        ))}
      </div>
      {trailerUrl && <YouTube videoId={trailerUrl} opts={opts} />}
    </div>
  );
}
